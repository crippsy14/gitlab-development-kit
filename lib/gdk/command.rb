# frozen_string_literal: true

module GDK
  module Command
    autoload :DiffConfig, 'gdk/command/diff_config'
    autoload :Doctor, 'gdk/command/doctor'
    autoload :Measure, 'gdk/command/measure'
    autoload :Help, 'gdk/command/help'
  end
end
